Rails.application.routes.draw do
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?
  get "category/:id",  to: "category_lists#index", as: "category"
  get 'items_log/index'
  get 'carts/index'
  post "carts/:id", to: "carts#create", as: "carts_add"
  get "login", to: "sessions#new"
  post "login", to: "sessions#create"
  delete "logout",to: "sessions#destroy"
  root 'static_pages#home'
  get '/help',to: 'static_pages#help'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets, only: [:new, :create, :edit, :update]
  resources :items
  resources :carts, only: [:destroy, :index]
  resources :profiles, only: [:edit, :update]
  resources :orders, only: [:new, :create, :destroy, :update]
  resources :order_cancels, only: [:update]
  resources :contacts, only: [:new, :create]
end
