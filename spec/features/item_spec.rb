require 'rails_helper'

RSpec.feature "Items", type: :feature do
  let(:user){ FactoryBot.create(:user) }
  let(:other_user){ FactoryBot.create(:other_user)}
  let(:item){ user.items.create(item_content: "aaaaa", title: "aaaa") }

  describe "destroy" do

    it "not correct user" do
      log_in_as(other_user)
      visit item_path(item)
      expect(page).not_to have_content "出品取り消し"
    end

  end
end
