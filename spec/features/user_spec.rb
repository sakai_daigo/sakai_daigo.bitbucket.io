require 'rails_helper'

RSpec.feature "Users", type: :feature do
  # pending "add some scenarios (or delete) #{__FILE__}"
  let(:user){ User.create(user_name: "nisikino", email: "nisikino@icloud.com", password: "1234567",
                                    password_confirmation: "1234567", user_phote: "default.jpg",
                                    background_phote: "background_default.png", activated: true) }

  describe "signup" do

    it "error request check" do
      visit new_user_path
      fill_in "ニックネーム", with: "tanaka"
      click_on "作成"
      # expect(current_path).to eq new_user_path 質問
      expect(page).to have_content "新規登録"
      expect(page).to have_selector "div#error_explanation"
    end

    it "success request check" do
      visit new_user_path
      fill_in "ニックネーム", with: "yamada"
      fill_in "メールアドレス", with: "yamada@icloud.com"
      fill_in "パスワード", with: "1234567"
      fill_in "パスワード確認", with: "1234567"
      click_on "作成"
      expect(current_path).to eq root_path
      expect(page).to have_content "メールを送信しました"
    end

  end

  describe "edit" do

    it "failure edit " do
      log_in_as(user)
      visit edit_user_path(user)
      fill_in "ニックネーム", with: " "
      click_on "変更"
      expect(page).to have_selector "div.alert-danger"
    end

    it "success edit" do
      log_in_as(user)
      visit edit_user_path(user)
      fill_in "ニックネーム", with: "foobar"
      click_on "変更"
      expect(page).to have_selector "div.alert-success"
      expect(user.reload.user_name).to eq "foobar"
    end

  end
end
