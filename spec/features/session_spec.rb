require 'rails_helper'

RSpec.feature "Sessions", type: :feature do

  let(:user){ User.create(user_name: "nisikino", email: "nisikino@icloud.com", password: "1234567",
                                    password_confirmation: "1234567", user_phote: "default.jpg",
                                    background_phote: "background_default.png", activated: true, ) }

  describe "login" do

    it "login pattern" do
      user
      log_in_as(user)
      expect(page).to have_selector "span.cart"
      expect(page).to have_selector "p.header-show-link"
      expect(page).to have_content "ログインしました"
      expect(page).not_to have_content "新規登録"
      expect(page).not_to have_selector "p.header-login-link"
    end

    it "not login pattern" do
      visit login_path
      fill_in "メールアドレス", with: " "
      fill_in "パスワード", with: ""
      click_button "ログイン"
      expect(page).not_to have_selector "a.cart"
      expect(page).not_to have_selector "div.header-show-link"
      expect(page).to have_content "新規登録"
      expect(page).to have_content "ログイン"
    end

  end

  describe "logout" do

    it "logout-link check" do
      log_in_as(user)
      click_on "ログアウト"
      expect(current_path). to eq root_path
      expect(page).to have_content "新規登録"
      expect(page).to have_selector "p.header-login-link"
    end

  end

end
