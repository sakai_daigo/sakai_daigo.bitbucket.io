require 'rails_helper'

RSpec.describe "Sessions", type: :request do

  let(:user){ User.create(user_name: "nisikino", email: "nisikino@icloud.com", password: "1234567",
                                    password_confirmation: "1234567", user_phote: "default.jpg",
                                    background_phote: "background_default.png",activated: true) }

  describe "logout" do

    it "two windows logout" do
      post login_path, params: { session: { email: user.email,
                                          password: "1234567" } }
      expect(response).to redirect_to user_path(user)
      expect(session[:user_id]).not_to eq nil
      delete logout_path
      expect(response).to redirect_to root_path
      delete logout_path
      expect(response).to redirect_to root_path
      expect(session[:user_id]).to eq nil
    end

  end

end
