require 'rails_helper'

RSpec.describe "Items", type: :request do
  let(:user){ FactoryBot.create(:user) }
  let(:other_user){ FactoryBot.create(:other_user)}
  let(:item){ user.items.create(item_content: "aaaaa", title: "aaaa") }

  describe "items validates" do

    it "validates check" do
      expect(item).to be_valid
    end

  end

  describe "item destroy" do

    describe "success" do

    it "correct user" do
      item
      sign_in_as(user)
      expect{ delete item_path(item.id) }.to change(Item, :count).by(-1)
      expect(response).to redirect_to root_url
    end

    end

    describe "failer" do

      it "not login" do
        item
        expect{ delete item_path(item.id) }.to change(Item, :count).by(0)
        expect(response).to redirect_to login_url
      end

      it "not correct user" do
        item
        sign_in_as(other_user)
        expect{ delete item_path(item.id) }.to change(Item, :count).by(0)
        expect(response).to redirect_to root_path
      end

    end

  end

end
