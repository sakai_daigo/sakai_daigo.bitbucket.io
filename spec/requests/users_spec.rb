require 'rails_helper'

RSpec.describe "Users", type: :request do

  let(:user){ FactoryBot.create(:user) }

  let(:other_user){ FactoryBot.create(:other_user) }

  describe "signup" do

    it "email_validation empty" do
      user.email = " "
      expect(user).to be_invalid
    end

    it "email_validation no format " do
      user.email = "aaaaaa "
      expect(user).to be_invalid
    end

    it "email_validation too long " do
      user.email = "aaasasa" * 250 + "@icloud.com"
      expect(user).to be_invalid
    end

    it "email_validation not uniqueness " do
      user
      other_user = User.create(user_name: "tanaka", email: "sakai@icloud.com",
                                    password: "password", password_confirmation: "password")
      expect(other_user).to be_invalid
    end

    it "user_name_validation empty" do
      user.user_name = " "
      expect(user).to be_invalid
    end

    it "user_name_validation too long" do
      user.user_name = "a" * 60
      expect(user).to be_invalid
    end

    it "password_validation empty" do
      user.password = " "
      expect(user).to be_invalid
    end

    it "password_validation too short" do
      user.password = "1234"
      expect(user).to be_invalid
    end

    # it "success response" do
    #   get new_user_path
    #   expect {
    #       post users_path, params: { user: { user_name: "user",
    #                                         email: "user@icloud.com",
    #                                         password: "1234567",
    #                                         password_confirmation: "1234567" } }
    #     }.to change(User, :count).by(1)
    #   expect(response).to redirect_to user_path(User.last.id)
    # end
  end

  describe "accussess restiction" do

    it "not login" do
      get edit_user_path(user)
      expect(response).to redirect_to login_url
      patch user_path(user), params: { user: { name: user.user_name,
                                              email: user.email } }
      expect(response).to redirect_to login_url
      expect{ delete user_path(user)}.to change(User, :count).by(0)
    end

    it "not correct user" do
      sign_in_as(other_user)
      get edit_user_path(user)
      expect(response).to redirect_to root_url
      patch user_path(user), params: { user: { name: user.user_name,
                                             email: user.email } }
      expect(response).to redirect_to root_url
      expect{ delete user_path(user)}.to change(User, :count).by(0)
    end

  it "successful edit with friendly forwarding" do
    get edit_user_path(user)
    sign_in_as(user)
    expect(response).to redirect_to edit_user_url(user)
  end

  end

  describe "destroy" do

    it "success destroy" do
      sign_in_as(user)
      expect{ delete user_path(user) }.to change(User, :count).by(-1)
    end

  end


end
