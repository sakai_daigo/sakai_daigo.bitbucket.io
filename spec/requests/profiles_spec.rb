require 'rails_helper'

RSpec.describe "Profiles", type: :request do
  describe "profiles validatetion" do

    let(:user){ FactoryBot.create(:user) }

    context "failuer pattern" do

      it "presence validate" do
        sign_in_as(user)
        get new_profile_path
        post profiles_path, params: { profile: { first_name: "", last_name: "", postal_code: "",
                                                    prefectures: "", city: "", address: "",
                                                    phone_number: "" }}
        expect(response.body).to include "お届け先登録"
      end

      it "format validate" do
        sign_in_as(user)
        get new_profile_path
        post profiles_path, params: { profile: { first_name: "tanaka", last_name: "yamato", postal_code: "123-456",
                                                    prefectures: "ニューヨーク", city: "枚方市宮沢区", address: "29-1",
                                                    phone_number: "09001234444" } }
        expect(response.body).to include "お届け先登録"
        get new_profile_path
        post profiles_path, params: { profile: { first_name: "tanaka", last_name: "yamato", postal_code: "1",
                                                    prefectures: "大阪", city: "枚方市宮沢区", address: "29-1",
                                                    phone_number: "09001234444" } }
        expect(response.body).to include "お届け先登録"
        get new_profile_path
        post profiles_path, params: { profile: { first_name: "tanaka", last_name: "yamato", postal_code: "123-4567",
                                                    prefectures: "大阪", city: "枚方市宮沢区", address: "29-1",
                                                    phone_number: "0" } }
        expect(response.body).to include "お届け先登録"
      end

    end

    context "success pattern" do

      it "login and correct request" do
        sign_in_as(user)
        get new_profile_path
        post profiles_path, params: { profile: { first_name: "tanaka", last_name: "yamato", postal_code: "123-4567",
                                                    prefectures: "大阪", city: "枚方市宮沢区", address: "29-1",
                                                    phone_number: "09001234444" } }
        expect(response).to redirect_to root_url
      end

    end
  end
end
