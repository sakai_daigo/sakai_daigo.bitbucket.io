require 'rails_helper'

RSpec.describe User, type: :model do

  let(:user){ User.create(user_name: "nisikino", email: "nisikino@icloud.com", password: "1234567",
                                    password_confirmation: "1234567", user_phote: "default.jpg",
                                    background_phote: "background_default.png") }

  it "authenticated? should return false for a user with nil digest" do
    expect(user.authenticated?("remember"," ")).to be_falsey
  end
end
