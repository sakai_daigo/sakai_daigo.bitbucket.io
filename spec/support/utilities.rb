
def log_in_as(user)
  visit login_path
  fill_in "メールアドレス", with: user.email
  fill_in "パスワード", with: "1234567"
  click_button "ログイン"
end

def sign_in_as(user)
  post login_path, params: { session: { email: user.email,
                                      password: "1234567" } }
end

def logged_in?
 !current_user.nil?
end
