# Preview all emails at http://localhost:3000/rails/mailers/contact_mailer
class ContactMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/contact_mailer/contact_mail
  def contact_mail
    contact = Contact.first
    ContactMailer.contact_mail(contact)
  end
  # def contact_mail
  #   ContactMailer.contact_mail
  # end

end
