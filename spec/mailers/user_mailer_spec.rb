require "rails_helper"

RSpec.describe UserMailer, type: :mailer do

  let(:user){ FactoryBot.create(:user) }

  describe "account_activation" do
    it "account_activation" do
      user.activation_token = User.new_token
      mail =  UserMailer.account_activation(user)
      # expect("アカウントの有効化").to eq mail.subject
      expect(mail.to).to eq [user.email]
      expect(mail.from).to eq ["noreply@example.com"]
      # expect(mail.body.encoded).to match user.user_name
      expect(mail.body.encoded).to match user.activation_token
      expect(mail.body.encoded).to match CGI.escape(user.email)
    end
  end

  describe "password_reset" do
    it "renders the headers" do
      user.reset_token = User.new_token
      mail =  UserMailer.password_reset(user)
      expect(mail.to).to eq [user.email]
      expect(mail.from).to eq ["noreply@example.com"]
      expect(mail.body.encoded).to match user.reset_token
      expect(mail.body.encoded).to match CGI.escape(user.email)
    end
  end

end
