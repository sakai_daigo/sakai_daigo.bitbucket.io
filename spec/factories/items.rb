FactoryBot.define do
  factory :item do
    title { "MyString" }
    item_content { "MyText" }
    item_phote { "MyString" }
    price { 1 }
    category { 1 }
    item_status { 1 }
    shipping_charge { 1 }
    type { 1 }
    days { 1 }
    sales { false }
    user { 1 }
  end
end
