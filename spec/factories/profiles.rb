FactoryBot.define do
  factory :profile do
    first_name { "MyString" }
    last_name { "MyString" }
    postcode { 1 }
    prefecture_code { 1 }
    address_city { "MyString" }
    address_street { "MyString" }
    address_building { "MyString" }
    user { nil }
    phone_number { 1 }
  end
end
