FactoryBot.define do

  factory :user do
    user_name { "sakai" }
    email { "sakai@icloud.com" }
    password { "1234567" }
    password_confirmation {"1234567"}
    activated { true }
    activated_at { Time.zone.now }
  end

  factory :other_user ,class: User do
    user_name { "yamada" }
    email { "yamada@icloud.com" }
    password { "1234567" }
    password_confirmation {"1234567"}
    activated { true }
    activated_at { Time.zone.now }
  end

end
