class AddPhoteToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :user_phote, :string
    add_column :users, :background_phote, :string
    add_column :users, :user_content, :text, null: true
  end
end
