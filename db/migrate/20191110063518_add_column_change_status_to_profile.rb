class AddColumnChangeStatusToProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :change_status, :boolean, default: false
  end
end
