class ChangeItemSalesDefault < ActiveRecord::Migration[5.2]
  def change
    change_column :items, :sales, :boolean, default: false
  end
end
