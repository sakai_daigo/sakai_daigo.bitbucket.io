class RemoveOneOrderTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :one_orders
  end
end
