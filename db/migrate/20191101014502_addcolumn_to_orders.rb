class AddcolumnToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :status, :boolean
    add_column :orders, :shippin_method, :integer
  end
end
