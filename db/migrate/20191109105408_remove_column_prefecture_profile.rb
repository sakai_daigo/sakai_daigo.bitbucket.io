class RemoveColumnPrefectureProfile < ActiveRecord::Migration[5.2]
  def change
    remove_column :profiles, :prefectures, :string
  end
end
