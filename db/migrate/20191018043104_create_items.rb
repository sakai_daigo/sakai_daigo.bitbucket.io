class CreateItems < ActiveRecord::Migration[5.2]

  def change
    create_table :items do |t|
      t.string :title
      t.text :item_content, null: true
      t.string :item_phote
      t.integer :price
      t.integer :category, default: 0
      t.integer :item_status, default: 0
      t.integer :shipping_charge, default: 0
      t.integer :type, default: 0
      t.integer :days
      t.boolean :sales
      t.references :user, foreign_key: true

      t.timestamps
    end
     add_index :items, [:user_id, :created_at]
  end

end
