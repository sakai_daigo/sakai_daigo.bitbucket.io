class AddColumnOrdersCancelStatus < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :cancel_status, :boolean, default: false
  end
end
