# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.delete_all

Item.delete_all


User.create!(user_name:  "aaa",
             email: "aaa@icloud.com",
             password:              "1234567",
             password_confirmation: "1234567",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

user = User.create!(user_name:  "sky-hi",
             email: "sky-hi@icloud.com",
             password:              "1234567",
             password_confirmation: "1234567",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

50.times do |n|

Item.create!(title: "机#{n+1}",
            item_content: "きれいな机です",
            item_phote: [File.open('spec/images/carten.jpg')],
            price: 1000,
            category: 0,
            item_status: 1,
            shipping_charge: 1,
            type: 1,
            days: 1,
            user_id: user.id
                )
end
