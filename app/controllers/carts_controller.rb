class CartsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :index]

  def create
    @item = Item.find_by(id: params[:id])
    @cart_item = current_user.carts.create(item_id: @item.id)
    redirect_to @item
  end

  def index
    @user = current_user
    @carts_item = current_user.carts
    @item = Item.all.order(created_at: :desc)
    @total_price = 0
    @carts_item.each do |carts|
      @total_price += carts.item.price.to_i
    end
  end

  def destroy
    @item = Item.find_by(id: params[:id])
    @cart_item = Cart.find_by(item_id: @item.id, user_id: current_user.id)
    @cart_item.destroy
    redirect_to @item
  end

end
