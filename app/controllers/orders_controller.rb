class OrdersController < ApplicationController
  before_action :profile_check
  before_action :logged_in_user

  def new
    @order = Order.new
    @profile = current_user.profile
    @one_order_item = Item.find_by(id: params[:format])
    if @one_order_item
      @total_price = @one_order_item.price.to_i
      @final_price = @total_price.to_i + 200
    else
      @carts_item = current_user.carts
      @total_price = 0
      @carts_item.each do |carts|
        @total_price += carts.item.price.to_i
      end
      @final_price = @total_price.to_i + 200
    end
  end

  def create
    @user = current_user
    if params[:item_id]
      @order = Order.new(shippin_method: order_params[:shippin_method], user_id: current_user.id, item_id: params[:item_id])
      @order.save
      @item = Item.find_by(id: params[:item_id])
      @item.update_attributes(sales: true)
      flash[:success] = "注文しました"
      redirect_to @user
    else
      current_user.carts.each do |carts|
        @order = Order.new(shippin_method: order_params[:shippin_method], user_id: current_user.id, item_id: carts.item_id )
        @order.save
        @item = Item.find_by(id: params[:item_id])
        @item.update_attributes(sales: true)
        flash[:success] = "注文しました"
        current_user.carts.destroy_all
      end
      redirect_to @user
    end
  end

  def update
    @user = current_user
    @order = Order.find_by(id: params[:id])
    @order.update_attributes(status: true)
    flash[:success] = "通知しました"
    redirect_to @user
  end

  def destroy
  end

  private

    def order_params
      params.require(:order).permit(:shippin_method, :format, :item_id )
    end

end
