class ItemsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :destroy]
  before_action :correct_user, only: [:destroy]
  before_action :profile_check, only: [:new]
  # before_action :already_soled_item, only: [:show]

  def new
    @item = current_user.items.build
  end

  def create
    # binding.pry
    @item = current_user.items.build(item_params)
    if @item.save
      flash[:success] = "出品しました"
      redirect_to @item
    else
      render "items/new"
    end
  end

  def show
    @item = Item.find_by(id: params[:id])
    @user = @item.user
    @items = @user.items
    @cart_item = Cart.new
    @order = Order.new
  end

  def destroy
    @item = Item.find_by(id: params[:id])
    @item.destroy
    flash[:success] = "出品を取り消しました"
    redirect_to root_url
  end

  def index
    @items = Item.page(params[:page]).per(30).search(params[:search]).order(created_at: :desc)
    @item_heading= params[:search]
    unless @item_heading
      @item_heading = "すべて"
    end
  end

  private

    def item_params
      params.require(:item).permit(:title, :item_content, :price, :category,
                                      :item_status, :shipping_charge, :type, :days,{item_phote: [] } )
    end

    def correct_user
      @item = current_user.items.find_by(id: params[:id])
      redirect_to root_url if @item.nil?
    end


end
