class OrderCancelsController < ApplicationController
  before_action :profile_check
  before_action :logged_in_user

  def update
    @user = current_user
    @order = Order.find_by(id: params[:id])
    @order.update_attributes(cancel_status: true)
    flash[:success] = "shere終了の手続きをしました"
    redirect_to @user
    # binding.pry
  end

end
