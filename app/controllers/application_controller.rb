class ApplicationController < ActionController::Base
  include SessionsHelper

  private

    def logged_in_user
     unless logged_in?
       store_location
       flash[:danger] = "ログインしてください"
       redirect_to login_url
     end
   end

   def profile_check
     unless current_user.profile.change_status
       redirect_to edit_profile_url(current_user.id)
     end
   end

   def already_soled_item
     if Order.find_by(item_id: params[:id])
       flash[:danger] = "売り切れです"
       redirect_to root_path
     end
   end

end
