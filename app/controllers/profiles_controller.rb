class ProfilesController < ApplicationController
  before_action :logged_in_user

  def edit
    @profile = current_user.profile
  end

  def update
    @profile = current_user.profile
    if @profile.change_status
      if @profile.update_attributes(profile_params)
        flash[:success] = "お届け先を変更しました"
        redirect_to new_order_url
      else
        render "profiles/edit"
      end
    else
      if @profile.update_attributes(first_name: profile_params[:first_name], last_name: profile_params[:last_name], postcode: profile_params[:postcode], prefecture_code: profile_params[:prefecture_code], address_city: profile_params[:address_city], address_street: profile_params[:address_street], address_building: profile_params[:address_building], phone_number: profile_params[:phone_number], change_status: true)
        flash[:success] = "登録しました"
        redirect_to root_url
      else
        @profile.change_status = false
        # 上のコードは問題ある？
        render "profiles/edit"
      end
    end
  end

  private

    def profile_params
      params.require(:profile).permit(:first_name, :last_name, :postcode, :prefecture_code,
                                        :address_city, :address_street, :address_building, :phone_number)
    end


end
