class ItemsLogController < ApplicationController

  def index
    @user = current_user
    @items = @user.items.page(params[:page]).per(10).order(created_at: :desc)
    @soled_item = []
    @not_soled_item = []
    @sipping_item = []
    @items.each do|item|
      @order = Order.find_by(item_id: item.id)
      if @order
        if @order.cancel_status == false
          @sipping_item.push(item)
        else
          @soled_item.push(item)
        end
      else
        @not_soled_item.push(item)
      end
    end
  end
end
