class UsersController < ApplicationController
  # before_action :admin_user,  only: :destroy
  before_action :logged_in_user, only: [:edit, :destroy, :show, :update]
  before_action :correct_user, only: [ :edit, :destroy, :show, :update]

  def new
    @user = User.new
  end

  def create
    @user=User.new(user_params)
    if @user.save
      UserMailer.account_activation(@user).deliver_now
      flash[:info] = "アカウント有効化のためのメールを送信しました。ご確認ください。"
      redirect_to root_url
    else
      render "new"
    end
  end

  def edit
    @user = User.find_by(id: params[:id])
  end

  def update
    @user = User.find_by(id: params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "編集しました"
      redirect_to @user
    else
      render "edit"
    end
  end

  def show
    @user = User.find_by(id: params[:id])
    @order = Order.new
    @before_item_history = current_user.orders.where(status: false)
    @after_item_history = current_user.orders.where(status: true)
    @cancel_item = current_user.orders.where(cancel_status: true)
  end

  def destroy
    User.find_by(id: params[:id]).destroy
    flash[:success] = "アカウントを削除しました。"
    redirect_to root_url
  end

  private

    def user_params
      params.require(:user).permit(:user_name, :email, :password,
                                      :password_confirmation, :background_phote, :user_phote, :user_content)
    end


   def correct_user
     @user = User.find_by(id: params[:id])
     redirect_to(root_url) unless @user == current_user
   end

    # def admin_user
    #   # redirect_to(root_url) unless current_user.admin?
    #
    # end

end
