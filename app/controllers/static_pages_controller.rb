class StaticPagesController < ApplicationController

  def home
    @new_item = Item.all.order(created_at: :desc)
    @furniture_item = Item.all.order(created_at: :desc)
    @not_solde_furniture_item = []
    @not_solde_new_item = []
    @furniture_item.each do |item|
      unless Order.find_by(item_id: item.id)
        @not_solde_furniture_item.push(item)
      end
    end
    @new_item.each do |item|
      unless Order.find_by(item_id: item.id)
        @not_solde_new_item.push(item)
      end
    end
  end

  def help
  end

end
