class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        flash[:success] = "ログインしました"
        log_in user
        remember(user)
        redirect_back_or user
      else
        message  = "アカウントが有効化されていません。 "
        message += "メールアドレスをチェックしてください"
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'パスワードとメールアドレスが一致しませんでした'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url #質問
  end

end
