class CategoryListsController < ApplicationController

  def index
    @items = Item.where(category: params[:id]).page(params[:page]).per(20).order(created_at: :desc)
    if @items.empty?
      flash[:danger] = "まだ出品された商品がありません"
      redirect_to root_path
    end
  end

end
