# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $("#profile_postcode").jpostal({
    postcode : [ "#profile_postcode" ],
    address  : {
                  "#profile_prefecture_code" : "%3",
                  "#profile_address_city"            : "%4",
                  "#profile_address_street"          : "%5%6%7"
                }
  })
