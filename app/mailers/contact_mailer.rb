class ContactMailer < ApplicationMailer

  def contact_mail(contact)
     @contact = contact
     mail to: ENV["KEY"], subject: @contact.name
  end

end
