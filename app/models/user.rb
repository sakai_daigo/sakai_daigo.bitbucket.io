class User < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  attr_accessor :remember_token, :activation_token, :reset_token
  after_create :profile_create
  before_save :downcase_email
  before_create :create_activation_digest
  has_one :profile, dependent: :destroy
  has_many :items, dependent: :destroy
  has_many :carts, dependent: :destroy
  has_many :orders, dependent: :destroy
  mount_uploader :user_phote, UserPhoteUploader
  mount_uploader :background_phote, BackgroundPhoteUploader
  validates :user_name, presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

    def password_reset_expired?
      reset_sent_at < 2.hours.ago
    end

    def User.digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                    BCrypt::Engine.cost
      BCrypt::Password.create(string, cost: cost)
    end

    def User.new_token
      SecureRandom.urlsafe_base64
    end

    def remember
      self.remember_token = User.new_token
      update_attribute(:remember_digest, User.digest(remember_token))
    end

    def authenticated?(attribute, token)
      digest = send("#{attribute}_digest")
      return false if digest.nil?
      BCrypt::Password.new(digest).is_password?(token)
    end

    def forget
      update_attribute(:remember_digest, nil)
    end

    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end

    def create_reset_digest
      self.reset_token = User.new_token
      update_columns(reset_digest:  User.digest(reset_token), reset_sent_at: Time.zone.now)
    end

    def downcase_email
      self.email = email.downcase
    end
 # パスワード再設定のメールを送信する
    def send_password_reset_email
      UserMailer.password_reset(self).deliver_now
    end

    def already_cart_exist?(item)
      self.carts.exists?(item_id: item.id)
    end

    def profile_create
      self.build_profile.save(validate: false)
    end

  # private

    # アップロードされた画像のサイズをバリデーションする
    # def picture_size
    #   if picture_size > 5.megabytes
    #     errors.add(:picture, "should be less than 5MB")
    #   end
    # end

end
