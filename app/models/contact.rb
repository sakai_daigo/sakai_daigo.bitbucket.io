class Contact < ApplicationRecord
  validates :email, presence: true, length: { maximum: 255 },
                   format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i }
  validates :message, presence: true
  validates :name, presence: true, length: { maximum: 50 }
  validates :title, presence: true, length: { maximum: 150 }
end
