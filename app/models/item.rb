class Item < ApplicationRecord
  belongs_to :user
  has_many :carts, dependent: :destroy
  has_many :orders, dependent: :destroy
  self.inheritance_column = :_type_disabled
  mount_uploaders :item_phote, ItemPhoteUploader
  validates :user_id, presence: true
  validates :item_content, length: { maximum: 250 }
  enum category: { furniture: 0, music: 1, accessories: 2, clothes: 3, book: 4, game: 5 }
  enum item_status: { new_item: 0, nodirt_item: 1, dirt_item: 2, bad_item: 3 }
  enum shipping_charge: { exhibitor_burden: 0, buyer_burden: 1 }

  def self.search(search)
    if search
      where(['title LIKE ?', "%#{search}%"]).where(sales: false)
    else
     all #全て表示。User.は省略
    end
  end

end
