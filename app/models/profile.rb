class Profile < ApplicationRecord
  VALID_PHONE_REGEX = /\A\d{10}$|^\d{11}\z/
  include JpPrefecture
  jp_prefecture :prefecture_code
  belongs_to :user
  validates_uniqueness_of :id, scope: :user_id
  validates :first_name, presence: true, length: { maximum: 50 }
  validates :last_name, presence: true, length: { maximum: 50 }
  # 郵便番号（ハイフンあり3桁・5桁・7桁）
  # validates :postcode, presence: true, format: { with: /\A\d{3}[-]\d{4}$|^\d{3}[-]\d{2}$|^\d{3}$|^\d{5}$|^\d{7}\Z/ }
  validates :address_city, presence: true
  validates :address_street, presence: true
  validates :phone_number, presence: true, uniqueness: true,  format: { with: VALID_PHONE_REGEX }

  def prefecture_name
    JpPrefecture::Prefecture.find(code: prefecture_code).try(:name)
  end

  def prefecture_name_find
    pref = JpPrefecture::Prefecture.find self.prefecture_code
    return pref.name
  end

  def prefecture_name=(prefecture_name)
    self.prefecture_code = JpPrefecture::Prefecture.find(name: prefecture_name).code
  end

end
